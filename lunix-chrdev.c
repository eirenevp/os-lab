/*
 * lunix-chrdev.c
 *
 * Implementation of character devices
 * for Lunix:TNG
 *
 * Iren Vlassi-Pandi, Yannis Chatzimichos
 *
 */

#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/cdev.h>
#include <linux/poll.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/mmzone.h>
#include <linux/vmalloc.h>
#include <linux/spinlock.h>
#include <linux/wait.h>

#include "lunix.h"
#include "lunix-chrdev.h"
#include "lunix-lookup.h"

/*
 * Global data
 */
struct cdev lunix_chrdev_cdev;

/*
 * Just a quick [unlocked] check to see if the cached
 * chrdev state needs to be updated from sensor measurements.
 */
static int lunix_chrdev_state_needs_refresh(struct lunix_chrdev_state_struct *state) {
    struct lunix_sensor_struct *sensor;

    WARN_ON (!(sensor = state->sensor));
    /* debug("\n\nentering %d\n\n", sensor->msr_data[state->type]->last_update != state->buf_timestamp); */

    return ( sensor->msr_data[state->type]->last_update != state->buf_timestamp );
}

/*
 * Updates the cached state of a character device
 * based on sensor data. Must be called with the
 * character device state lock held.
 */
static int lunix_chrdev_state_update(struct lunix_chrdev_state_struct *state) {
    struct lunix_sensor_struct *sensor;
    uint32_t data, timestamp;
    enum lunix_msr_enum type;
    int ret, i;
    long value, int_part, frac_part;
    unsigned long flags;

    /* debug("entering\n"); */

    sensor = state->sensor;
    type = state->type;
    /*
     * Grab the raw data quickly, hold the
     * spinlock for as little as possible.
     */

    /* Why use spinlocks? See LDD3, p. 119 */
    /* lock_irqsave disables interrupts
     * (on the local processor only) */
    spin_lock_irqsave(&sensor->lock, flags);
    data = sensor->msr_data[type]->values[0];
    timestamp = sensor->msr_data[type]->last_update;
    spin_unlock_irqrestore(&sensor->lock, flags);

    /*
     * Any new data available?

     * Now we can take our time to format them,
     * holding only the private state semaphore
     */

    /* debug("--> lunix_chrdev_state_needs_refresh result: %d", lunix_chrdev_state_needs_refresh(state)); */
    if ( lunix_chrdev_state_needs_refresh(state) ) {
        state->buf_timestamp = timestamp;

        switch (type) {
            case BATT:
                value = lookup_voltage[data];
                break;
            case TEMP:
                value = lookup_temperature[data];
                break;
            case LIGHT:
                value = lookup_light[data];
                break;
            default:
                break;
        }

        int_part = value/1000;
        frac_part = value%1000;
        state->buf_lim = 0;
        state->buf_data[state->buf_lim++] = '\n';

        for (i=0; i<3; ++i) {
            state->buf_data[state->buf_lim++] = frac_part%10 + '0';
            frac_part /= 10;
        }
        state->buf_data[state->buf_lim++] = '.';
        if (int_part == 0)
            state->buf_data[state->buf_lim++] = '0';
        while (int_part > 0) {
            state->buf_data[state->buf_lim++] = int_part%10 + '0';
            int_part /= 10;
        }
        state->buf_data[state->buf_lim++] = (value > 0 ? '+' : '-');
        /*
         * metrisi: 12.345
         * buf_data = '\n', '5', '4', '3', '.', '2', '1', '+'
         * buf_lim = 8
         diavazei 2 xaraktires:
         * buf_lim -= 2
         * buf_data = '\n', '5', '4', '3', '.', '2'
         */
        ret = 0;
    }
    else {
        /* No new data */
        ret = -EAGAIN;
    }

    debug("leaving, exit code: %d\n", ret);
    return ret;
}

/*************************************
 * Implementation of file operations
 * for the Lunix character device
 *************************************/

static int lunix_chrdev_open(struct inode *inode, struct file *filp) {
    /* Declarations */
    int minor_number;
    int sensor_no;
    enum lunix_msr_enum sensor_type;
    int ret;
    struct lunix_chrdev_state_struct *state;

    debug("entering\n");
    ret = -ENODEV;
    if ((ret = nonseekable_open(inode, filp)) < 0)
        goto out;

    /*
     * Associate this open file with the relevant sensor based on
     * the minor number of the device node [/dev/sensor<NO>-<TYPE>]
     */
    minor_number = iminor(inode);
    sensor_no = minor_number>>3;
    sensor_type = minor_number&7;

    /* Allocate a new Lunix character device private state structure */

    state = kzalloc(sizeof(struct lunix_chrdev_state_struct), GFP_KERNEL);
    state->sensor = &lunix_sensors[sensor_no];
    state->type = sensor_type;

    filp->private_data = state;

    init_MUTEX(&state->lock);

    ret = 0;
out:
    debug("leaving, with ret = %d\n", ret);
    return ret;
}

static int lunix_chrdev_release(struct inode *inode, struct file *filp) {
    /*
     * kaleitai gia to kleisimo tou anoixtou arxeiou, opote
     * katastrefetai h antistoixh domh file
     * gia na enhmerwnomaste otan to programma pou exei anoiksei 
     * th syskeyh mas kleisei to antistoixo arxeio kai na 
     * eleutherwsoume tou antistoixous porous (desmeumenh mnhmh)
     */

    /* Deallocate anything that open allocated in filp->private_data */
    kfree(filp->private_data);
    return 0;
}

static long lunix_chrdev_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {
    /* Why? */
    return -EINVAL;
}

static ssize_t lunix_chrdev_read(struct file *filp, char __user *usrbuf, size_t cnt, loff_t *f_pos) {
    ssize_t ret = 0;
    int idx, count;
    char buf[LUNIX_CHRDEV_BUFSZ];

    struct lunix_sensor_struct *sensor;
    struct lunix_chrdev_state_struct *state;

    state = filp->private_data;
    WARN_ON(!state);

    sensor = state->sensor;
    WARN_ON(!sensor);

    debug("entering");

    /* Lock */
    if ( down_interruptible(&state->lock) )
        return -ERESTARTSYS;

    debug("got the lock %d", state->buf_lim);
    /*
     * If the cached character device state needs to be
     * updated by actual sensor data (i.e. we need to report
     * on a "fresh" measurement, do so
     */
    if (state->buf_lim == 0) {
        while (lunix_chrdev_state_update(state) == -EAGAIN) {
            /* drop the device semaphore */
            up(&state->lock);
            /* check for non-blocking I/O */
            if (filp->f_flags & O_NONBLOCK)
                return -EAGAIN;
            debug("Reading: going to sleep");
            /* The process needs to sleep */
            /* See LDD3, page 153 for a hint */
            if ( wait_event_interruptible(state->sensor->wq,
                        lunix_chrdev_state_needs_refresh(state)) )
                return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
            /* otherwise loop, but first reacquire the lock */
            /* someone else might win the race and get the data first! */
            if ( down_interruptible(&state->lock) )
                return -ERESTARTSYS;
        }
    }
    /* semaphore is held and the buffer contains data that we can use */ 

    /* debug("ready to return the data: I have: %d, Requested: %d", state->buf_lim, cnt); */
    if (state->buf_lim == 0) {
        ret = -EFAULT;
        goto out;
    }

    count = (state->buf_lim >= cnt ? cnt : state->buf_lim);

    for(idx = 0; idx < count; ++idx) {
        buf[idx] = state->buf_data[state->buf_lim - idx - 1];
    }
    state->buf_lim -= count;

    if (copy_to_user(usrbuf, buf, count)) {
        ret = -EFAULT;
        goto out;
    }

    ret = count;

out:
    /* Unlock */
    up(&state->lock);
    return ret;
}

static int lunix_chrdev_mmap(struct file *filp, struct vm_area_struct *vma) {
    return -EINVAL;
}

static struct file_operations lunix_chrdev_fops = {
    .owner          = THIS_MODULE,
    .open           = lunix_chrdev_open,
    .release        = lunix_chrdev_release,
    .read           = lunix_chrdev_read,
    .unlocked_ioctl = lunix_chrdev_ioctl,
    .mmap           = lunix_chrdev_mmap
};

int lunix_chrdev_init(void) {
    /*
     * Register the character device with the kernel, asking for
     * a range of minor numbers (number of sensors * 8 measurements / sensor)
     * beginning with LUNIX_CHRDEV_MAJOR:0
     */
    int ret;
    dev_t dev_no;
    unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;

    debug("initializing character device\n");
    cdev_init(&lunix_chrdev_cdev, &lunix_chrdev_fops);
    lunix_chrdev_cdev.owner = THIS_MODULE;

    dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
    ret = register_chrdev_region(dev_no, lunix_minor_cnt, "lunix");

    if (ret < 0) {
        debug("failed to register region, ret = %d\n", ret);
        goto out;
    }

    ret = cdev_add(&lunix_chrdev_cdev, dev_no, lunix_minor_cnt);
    if (ret < 0) {
        debug("failed to add character device\n");
        goto out_with_chrdev_region;
    }
    debug("completed successfully\n");
    return 0;

out_with_chrdev_region:
    unregister_chrdev_region(dev_no, lunix_minor_cnt);
out:
    return ret;
}

void lunix_chrdev_destroy(void) {
    dev_t dev_no;
    unsigned int lunix_minor_cnt = lunix_sensor_cnt << 3;

    debug("entering\n");
    dev_no = MKDEV(LUNIX_CHRDEV_MAJOR, 0);
    cdev_del(&lunix_chrdev_cdev);
    unregister_chrdev_region(dev_no, lunix_minor_cnt);
    debug("leaving\n");
}
